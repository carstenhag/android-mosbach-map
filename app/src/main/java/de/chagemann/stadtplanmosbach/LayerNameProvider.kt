package de.chagemann.stadtplanmosbach

import android.content.Context

class LayerNameProvider(private val context: Context) {

    private val map: Map<String, String> = mapOf(
            Pair("app-waste-basket", context.getString(R.string.layer_description_waste_basket)),
            Pair("app-bicycle-parking", context.getString(R.string.layer_description_bicycle_parking)),
            Pair("app-atm", context.getString(R.string.layer_description_atm)),
            Pair("app-postboxes", context.getString(R.string.layer_description_postboxes)),
            Pair("app-playgrounds", context.getString(R.string.layer_description_playgrounds)),
            Pair("app-toilets", context.getString(R.string.layer_description_toilets)),
            Pair("app-parking", context.getString(R.string.layer_description_parking)),
            Pair("app-recycling", context.getString(R.string.layer_description_recycling))
    )

    fun getLayerName(layerID: String): String {
        return map[layerID] ?: context.getString(R.string.layer_description_unknown)
    }

}