package de.chagemann.stadtplanmosbach

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainActivityViewModel : ViewModel() {

    val isSatelliteLayerVisible = MutableLiveData<Boolean>(false)

}