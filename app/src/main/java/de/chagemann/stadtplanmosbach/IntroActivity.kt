package de.chagemann.stadtplanmosbach

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.core.app.ActivityOptionsCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.github.paolorotolo.appintro.AppIntro
import com.github.paolorotolo.appintro.AppIntroFragment
import com.github.paolorotolo.appintro.model.SliderPage

class IntroActivity : AppIntro() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        getSharedPreferences(packageName, Context.MODE_PRIVATE).apply {
            if (!getBoolean(getString(R.string.preferences_is_first_start), true)) {
                startMainActivity()
            }
        }

        isSkipButtonEnabled = false
        showSeparator(false)

        addSlide(
            AppIntroFragment.newInstance(
                SliderPage(
                    getString(R.string.intro_1_title),
                    getString(R.string.intro_1_description),
                    imageDrawable = R.drawable.ic_place_outline,
                    bgColor = ContextCompat.getColor(this, R.color.intro1Background)
                )
            )
        )

        addSlide(
            AppIntroFragment.newInstance(
                SliderPage(
                    getString(R.string.intro_2_title),
                    getString(R.string.intro_2_description),
                    imageDrawable = R.drawable.mapbox_compass_icon,
                    bgColor = ContextCompat.getColor(this, R.color.intro2Background)
                )
            )
        )

        setColorDoneText(ContextCompat.getColor(this, R.color.intro1Background))
    }

    override fun onDonePressed(currentFragment: Fragment?) {
        super.onDonePressed(currentFragment)
        startMainActivity()
    }

    private fun startMainActivity() {
        getSharedPreferences(packageName, Context.MODE_PRIVATE).apply {
            edit().putBoolean(getString(R.string.preferences_is_first_start), false).apply()
        }

        val intent = MainActivity.newIntent(this)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        // https://stackoverflow.com/a/43777084/3991578
        val bundle = ActivityOptionsCompat.makeCustomAnimation(applicationContext,
            android.R.anim.fade_in, android.R.anim.fade_out
        ).toBundle()
        startActivity(intent, bundle)
    }
}
