package de.chagemann.stadtplanmosbach.extensions

import com.mapbox.mapboxsdk.maps.MapboxMap

fun MapboxMap.toggleMapGestures() {
    uiSettings.apply {
        isDoubleTapGesturesEnabled = !isDoubleTapGesturesEnabled
        isScrollGesturesEnabled = !isScrollGesturesEnabled
        isZoomGesturesEnabled = !isZoomGesturesEnabled
    }
}