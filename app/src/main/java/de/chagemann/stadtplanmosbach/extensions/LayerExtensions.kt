package de.chagemann.stadtplanmosbach.extensions

import android.animation.ValueAnimator
import com.mapbox.mapboxsdk.style.layers.Layer
import com.mapbox.mapboxsdk.style.layers.PropertyFactory
import de.chagemann.stadtplanmosbach.MainActivity

fun Layer.animateOpacity(initialValue: Float, finalValue: Float) {
    ValueAnimator.ofFloat(initialValue, finalValue).apply {
        duration = MainActivity.COMMON_ANIMATION_DURATION
        addUpdateListener {
            this@animateOpacity.setProperties(PropertyFactory.rasterOpacity(it.animatedValue as? Float))
        }
        start()
    }
}