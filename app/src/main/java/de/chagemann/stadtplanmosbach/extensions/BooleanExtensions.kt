package de.chagemann.stadtplanmosbach.extensions

import com.mapbox.mapboxsdk.style.layers.Property
import com.mapbox.mapboxsdk.style.layers.PropertyFactory
import com.mapbox.mapboxsdk.style.layers.PropertyValue

fun Boolean.toMapboxVisibilityProperty(): PropertyValue<String> {
    return when (this) {
        true -> PropertyFactory.visibility(Property.VISIBLE)
        false -> PropertyFactory.visibility(Property.NONE)
    }
}