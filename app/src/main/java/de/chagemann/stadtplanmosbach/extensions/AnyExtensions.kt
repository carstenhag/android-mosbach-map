package de.chagemann.stadtplanmosbach.extensions

// https://stackoverflow.com/a/53510106/3991578
val Any.TAG: String
    get() {
        val tag = javaClass.simpleName
        return if (tag.length <= 23) tag else tag.substring(0, 23)
    }