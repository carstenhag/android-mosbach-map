package de.chagemann.stadtplanmosbach

import android.Manifest
import android.animation.ValueAnimator
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.crashlytics.android.Crashlytics
import com.github.florent37.runtimepermission.kotlin.askPermission
import com.google.android.material.chip.Chip
import com.mapbox.android.core.permissions.PermissionsManager
import com.mapbox.api.geocoding.v5.GeocodingCriteria
import com.mapbox.api.geocoding.v5.MapboxGeocoding
import com.mapbox.api.geocoding.v5.models.GeocodingResponse
import com.mapbox.geojson.Point
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.location.LocationComponentActivationOptions
import com.mapbox.mapboxsdk.location.LocationComponentOptions
import com.mapbox.mapboxsdk.location.modes.CameraMode
import com.mapbox.mapboxsdk.location.modes.RenderMode
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.MapboxMap.OnMapLongClickListener
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback
import com.mapbox.mapboxsdk.maps.Style
import com.mapbox.mapboxsdk.style.layers.Layer
import de.chagemann.stadtplanmosbach.extensions.TAG
import de.chagemann.stadtplanmosbach.extensions.animateOpacity
import de.chagemann.stadtplanmosbach.extensions.toMapboxVisibilityProperty
import de.chagemann.stadtplanmosbach.extensions.toggleMapGestures
import io.fabric.sdk.android.Fabric
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

class MainActivity : AppCompatActivity(), OnMapReadyCallback, OnMapLongClickListener {

    private lateinit var viewModel: MainActivityViewModel

    private lateinit var mapboxMap: MapboxMap

    private var savedInstanceState: Bundle? = null

    private var animator: ValueAnimator? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Mapbox.getInstance(this, getString(R.string.MAPBOX_ANDROID_TOKEN))
        setContentView(R.layout.activity_main)
        Fabric.with(this, Crashlytics())
        this.savedInstanceState = savedInstanceState

        afterViews()
    }

    private fun afterViews() {
        viewModel = ViewModelProvider(this).get(MainActivityViewModel::class.java)

        mapView.onCreate(this.savedInstanceState)
        mapView.getMapAsync(this)

        toggleSatelliteButton.setOnClickListener { onSatelliteToggleButton() }

        viewModel.isSatelliteLayerVisible.observe(this, Observer { visible ->
            setSatelliteLayerVisibility(visible)
        })
    }

    override fun onMapReady(mapboxMap: MapboxMap) {
        this.mapboxMap = mapboxMap
        setupMap()
    }

    private fun getCustomizableLayers(mapboxStyle: Style?): List<Layer> {
        return mapboxStyle?.layers?.filter { layer ->
            layer.id.contains("app")
        } ?: listOf()
    }

    private fun askForPermission() {
        askPermission(Manifest.permission.ACCESS_FINE_LOCATION) {
            enableLocationComponent()
        }.onDeclined { e ->
            when {
                e.hasForeverDenied() -> AlertDialog.Builder(this@MainActivity)
                        .setTitle(getString(R.string.alert_location_permission_title))
                        .setMessage(getString(R.string.alert_location_permission_forever_denied_message))
                        .setPositiveButton(getString(R.string.alert_location_permission_forever_denied_positive)) { _, _ ->
                            e.goToSettings()
                        }
                        .setNegativeButton(getString(R.string.alert_cancel_negative)) { _, _ ->
                            return@setNegativeButton
                        }
                        .show()
                e.hasDenied() -> AlertDialog.Builder(this@MainActivity)
                        .setTitle(getString(R.string.alert_location_permission_title))
                        .setMessage(getString(R.string.alert_location_permission_once_denied_message))
                        .setPositiveButton(getString(R.string.alert_location_permission_once_denied_positive)) { _, _ ->
                            askForPermission()
                        }
                        .setNegativeButton(getString(R.string.alert_cancel_negative)) { _, _ ->
                            return@setNegativeButton
                        }
                        .show()
            }
        }
    }


    private fun setupMap() {
        fun createChipView(layer: Layer): Chip {
            return Chip(this@MainActivity).apply {
                // Can't use a style for this, sadly: https://issuetracker.google.com/issues/118392623
                isCheckable = true
                isChipIconVisible = false
                isCloseIconVisible = false
                @SuppressLint("PrivateResource")
                checkedIcon = getDrawable(R.drawable.ic_mtrl_chip_checked_black)
                //chipBackgroundColor = ContextCompat.getColorStateList(this@MainActivity, R.drawable.chip_background_color)
                //setTextColor(resources.getColor(android.R.color.white, null))

                text = LayerNameProvider(context).getLayerName(layer.id)
                isChecked = true

                setOnCheckedChangeListener { _, isChecked ->
                    layer.setProperties(isChecked.toMapboxVisibilityProperty())
                }
            }
        }

        mapboxMap.apply {
            setStyle(Style.Builder().fromUri(MAP_STYLE_URL)) { style ->
                activateLocationComponent()
                askForPermission()

                getCustomizableLayers(style).map {
                    createChipView(it)
                }.forEach {
                    filterChipGroup.addView(it)
                }
            }

            addOnMapLongClickListener(this@MainActivity)
            addOnCameraIdleListener(idleListener)

            setMinZoomPreference(MIN_ZOOM_LEVEL)
            setMaxZoomPreference(MAX_ZOOM_LEVEL)
            uiSettings.isRotateGesturesEnabled = false
            uiSettings.isTiltGesturesEnabled = false

            moveCamera(CameraUpdateFactory.zoomTo(INITIAL_ZOOM_LEVEL))
            easeCamera(CameraUpdateFactory.zoomBy(ANIMATION_ZOOM_CONSTANT), COMMON_ANIMATION_DURATION.toInt())
        }
    }

    /**
     * Only show custom layers when zoomed in enough, else the map lags a lot.
     */
    private val idleListener = MapboxMap.OnCameraIdleListener {
        val layers = getCustomizableLayers(mapboxMap.style)
        val shouldShowLayers = mapboxMap.cameraPosition.zoom >= LAYER_CUTOFF_ZOOM_LEVEL
        layers.forEach {
            it.setProperties(shouldShowLayers.toMapboxVisibilityProperty())
        }
    }

    private fun activateLocationComponent() {
        val locationComponent = mapboxMap.locationComponent
        mapboxMap.style?.let { style ->
            val locationComponentOptions = LocationComponentOptions.builder(this).build()
            val activationOptions = LocationComponentActivationOptions.Builder(this@MainActivity, style)
                    .locationComponentOptions(locationComponentOptions)
                    .build()
            locationComponent.activateLocationComponent(activationOptions)
        }

        locationComponent.renderMode = RenderMode.COMPASS
        locationComponent.cameraMode = CameraMode.TRACKING_GPS_NORTH

        val animatedOptions = LocationComponentOptions.builder(this@MainActivity)
        animator = ValueAnimator.ofFloat(1f, LOCATION_COMPONENT_MAX_ZOOM_FACTOR).apply {
            addUpdateListener {
                locationComponent.applyStyle(animatedOptions.minZoomIconScale(it.animatedValue as Float).build())
            }
            interpolator = AccelerateDecelerateInterpolator()
            duration = 1000
            repeatMode = ValueAnimator.REVERSE
            repeatCount = ValueAnimator.INFINITE
        }

        locationComponent.addOnLocationStaleListener { stale ->
            if (stale && animator?.isRunning == true) {
                animator?.cancel()
            } else {
                animator?.start()
            }
        }

        locationComponent.addOnLocationClickListener {
            val lastLocation = locationComponent.lastKnownLocation
                               ?: return@addOnLocationClickListener
            val client = MapboxGeocoding.builder()
                    .accessToken(getString(R.string.MAPBOX_ANDROID_TOKEN))
                    .query(Point.fromLngLat(lastLocation.longitude, lastLocation.latitude))
                    .geocodingTypes(GeocodingCriteria.TYPE_ADDRESS)
                    .build()

            client.enqueueCall(object : Callback<GeocodingResponse> {
                override fun onResponse(call: Call<GeocodingResponse>, response: Response<GeocodingResponse>) {
                    val results = response.body()?.features() ?: listOf()
                    if (results.size > 0) {
                        val feature = results[0]
                        Toast.makeText(
                            this@MainActivity,
                            String.format(Locale.US, getString(R.string.current_address_string), feature.text(), feature.address()),
                            Toast.LENGTH_SHORT
                        ).show()
                    } else {
                        Log.w(TAG, getString(R.string.log_info_geocoding_no_result))
                    }
                }

                override fun onFailure(call: Call<GeocodingResponse>, t: Throwable) {
                    Log.w(TAG, getString(R.string.log_info_geocoding_failed), t)
                }
            })
        }
    }

    // Currently does not show up when GPS/Location is disabled on the device.
    // A prompt should make the user enable location services again.
    private fun enableLocationComponent() {
        if (!PermissionsManager.areLocationPermissionsGranted(this)) {
            Log.e(TAG, "Permissions should have, but were not granted")
            return
        }

        val locationComponent = mapboxMap.locationComponent
        locationComponent.isLocationComponentEnabled = true
    }

    private fun onSatelliteToggleButton() {
        viewModel.isSatelliteLayerVisible.postValue(
            !(viewModel.isSatelliteLayerVisible.value ?: false)
        )
    }

    private fun setSatelliteLayerVisibility(visible: Boolean) {
        if (!::mapboxMap.isInitialized) {
            Log.w(TAG, "MapboxMap not initialized")
            return
        }

        val satelliteLayer = mapboxMap.style?.getLayer("mapbox-satellite")
        if (satelliteLayer == null) {
            Log.e(TAG, "SatelliteLayer could not be found")
            return
        }

        toggleSatelliteButton.apply {
            isClickable = false
            alpha = .5f
            postDelayed({
                isClickable = true
                alpha = 1f
            }, COMMON_ANIMATION_DURATION)
        }

        if (!visible) {
            satelliteLayer.animateOpacity(1f, 0f)
            mapboxMap.setMaxZoomPreference(MAX_ZOOM_LEVEL)
        } else {
            // Needed because Mapbox sets the visibility to NONE when the opacity is at 0f
            satelliteLayer.setProperties(true.toMapboxVisibilityProperty())
            satelliteLayer.animateOpacity(0f, 1f)

            mapboxMap.apply {
                if (cameraPosition.zoom > MAX_ZOOM_LEVEL_SATELLITE) {
                    easeCamera(CameraUpdateFactory.zoomTo(MAX_ZOOM_LEVEL_SATELLITE), COMMON_ANIMATION_DURATION.toInt())
                }
                toggleMapGestures()
                Handler().postDelayed({
                    setMaxZoomPreference(MAX_ZOOM_LEVEL_SATELLITE)
                    toggleMapGestures()
                }, COMMON_ANIMATION_DURATION)
            }
        }
    }

    override fun onMapLongClick(point: LatLng): Boolean {
        Toast.makeText(this, point.toString(), Toast.LENGTH_SHORT)
        AlertDialog.Builder(this@MainActivity)
                .setTitle(getString(R.string.alert_navigation_title))
                .setMessage(getString(R.string.alert_navigation_description))
                .setPositiveButton(getString(android.R.string.yes)) { _, _ ->
                    val uri = Uri.parse("geo:0,0?q=" + point.latitude + "," + point.longitude)
                    val intent = Intent(Intent.ACTION_VIEW, uri)
                    startActivity(intent)
                }
                .setNegativeButton(getString(android.R.string.cancel)) { di, _ ->
                    di.dismiss()
                }
                .setCancelable(true)
                .show()
        return true
    }

    public override fun onStart() {
        super.onStart()
        mapView.onStart()
    }

    public override fun onResume() {
        super.onResume()
        mapView.onResume()

        if (::mapboxMap.isInitialized && !mapboxMap.locationComponent.isLocationComponentActivated) {
            enableLocationComponent()
        }
    }

    public override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    public override fun onStop() {
        super.onStop()
        mapView.onStop()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

    override fun onDestroy() {
        super.onDestroy()
        animator?.cancel()
        mapView.onDestroy()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mapView.onSaveInstanceState(outState)
    }

    companion object {
        private const val MAP_STYLE_URL = "mapbox://styles/carstenhag/cjrgogwjl0j9r2sutxidpt9i2"

        private const val INITIAL_ZOOM_LEVEL = 13.0
        private const val ANIMATION_ZOOM_CONSTANT = 1.75
        private const val MIN_ZOOM_LEVEL = 7.0
        private const val MAX_ZOOM_LEVEL = 20.0
        private const val MAX_ZOOM_LEVEL_SATELLITE = 17.0

        // The map lags a lot when zoomed out (below this constant), so we only show the
        // custom layers with a zoom factor of this or higher.
        private const val LAYER_CUTOFF_ZOOM_LEVEL = 12.0

        private const val LOCATION_COMPONENT_MAX_ZOOM_FACTOR = 1.25f

        const val COMMON_ANIMATION_DURATION = 750L

        fun newIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }
}