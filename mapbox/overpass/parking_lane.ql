[timeout:3600];
( area["ISO3166-1:alpha3"="DEU"];) ->.a;
    (
        node["parking:lane"]["parking:lane" != "no_parking"]
        (area.a);

        way["parking:lane"]["parking:lane" != "no_parking"]
        (area.a);

        relation["parking:lane"]["parking:lane" != "no_parking"]
        (area.a);
    );
    (._;>;);
 out body;